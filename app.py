import os
from collections import OrderedDict
from itertools import compress

import boto3
import face_recognition
import imagehash
from PIL import Image
from flask import Flask, jsonify, request, redirect

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'gif'}

app = Flask(__name__)


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/', methods=['POST'])
def identify_faces():
    # Check if a valid image file was uploaded
    if 'file' not in request.files:
        return redirect(request.url)

    file = request.files['file']

    if file.filename == '':
        return redirect(request.url)

    if file and allowed_file(file.filename):
        # The image file seems valid! Detect faces and return the result.
        match_results, _ = detect_faces_in_image(file, tolerance=TOLERANCE)
        # take only first for simplicity now
        if any(match_results):
            result = get_info_from_match(match_results)
            return jsonify(result)
        else:
            return jsonify({'message': 'unknown face'})


def get_info_from_match(match_results):
    who = list(compress(list(KNOWN_FACE_ENCODING.keys()), match_results))[0]
    result = DYNAMODB_CLIENT.get_item(TableName=DYNAMODB_TABLENAME,
                                      Key={'star_identifier': {'S': who}}).get('Item')
    result.pop('star_identifier')
    return result


@app.route('/known_faces', methods=['POST'])
def add_faces():
    if 'file' not in request.files:
        return redirect(request.url)

    file = request.files['file']
    if file.filename == '':
        return redirect(request.url)

    if file and allowed_file(file.filename):
        match_results, encoding = detect_faces_in_image(file, tolerance=TOLERANCE)
        if any(match_results):
            # known result
            result = get_info_from_match(match_results)
            return jsonify(result)
        else:
            # insert into s3 and dynamo and KNOWN_FACE_ENCODING
            with Image.open(file) as img:
                phash = str(imagehash.phash(img))
            face_info = dict(request.values)

            global KNOWN_FACE_ENCODING
            KNOWN_FACE_ENCODING[phash] = encoding

            file.seek(0)
            S3_CLIENT.put_object(Body=file, Bucket=BUCKET_NAME, Key=phash)

            dynamo_item = {k: {'S': v} for k, v in face_info.items()}
            dynamo_item['star_identifier'] = {'S': phash}
            dynamo_item['s3_path'] = {'S': 's3://' + BUCKET_NAME + '/' + phash}
            DYNAMODB_CLIENT.put_item(Item=dynamo_item,
                                     TableName=DYNAMODB_TABLENAME)
            return jsonify({'message': 'new known face inserted'})


def detect_faces_in_image(file_stream, tolerance=0.6):
    img = face_recognition.load_image_file(file_stream)
    unknown_face_encodings = face_recognition.face_encodings(img)

    if len(unknown_face_encodings) == 0:
        raise Exception("no faces found")

    return face_recognition.compare_faces(list(KNOWN_FACE_ENCODING.values()), unknown_face_encodings[0],
                                          tolerance=tolerance), unknown_face_encodings[0]


def generate_encodings():
    encodings_dict = OrderedDict()
    for s3_object in S3_CLIENT.list_objects(Bucket=BUCKET_NAME).get('Contents', []):
        # TODO currently will just redo everything, should make it smarter.
        filename = s3_object.get('Key')
        object_body = S3_CLIENT.get_object(Bucket=BUCKET_NAME, Key=filename).get('Body')
        img = face_recognition.load_image_file(object_body)
        # only 1 face per image
        encodings_dict[filename] = face_recognition.face_encodings(img)[0]

    return encodings_dict


if __name__ == "__main__":
    TOLERANCE = float(os.environ.get('TOLERANCE', 0.6))
    DYNAMODB_CLIENT = boto3.client('dynamodb', endpoint_url=os.environ.get('DYNAMODB_ENDPOINT'))
    DYNAMODB_TABLENAME = os.environ.get('DYNAMODB_TABLENAME')
    S3_CLIENT = boto3.client('s3', endpoint_url=os.environ.get('S3_ENDPOINT'))
    BUCKET_NAME = os.environ.get('S3_BUCKET')
    if not BUCKET_NAME:
        raise Exception('Need to provide S3 bucket name of where the known face data')

    KNOWN_FACE_ENCODING = generate_encodings()

    app.run(host='0.0.0.0', port=5001, debug=False)
