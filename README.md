# Face Recognition
An API that allows you to retrieve information from your database based on faces.

Built with the work from https://github.com/ageitgey/face_recognition. It uses S3 to store the known images and dynamodb to store the information you want to save per person.

## Trying Locally
You can start simply by running 
```bash
./local_start.sh
```

You can send a request to the service by
```bash
curl -XPOST -F "file=@example-images/aaron-1.jpg" http://127.0.0.1:5001
```

To add new faces and other details:
```bash
curl -XPOST -F "file=@example-images/andy.jpg" -F "body=123" -F "blah=456" http://127.0.0.1:5001/known_faces
```

## TODOs
- upload to S3 doesn't work!
- force syncronization between S3 and dynamodb
- provide an easier way to update the databases
- allow multiple faces per picture when validating
- less hardcoded bash script
- allow multiple pictures per known person
- tuning for Asian faces