#!/usr/bin/env bash

DC=( docker-compose -f docker-compose.yml )
S3_ENDPOINT=http://localhost:4572
S3_BUCKET=hkstars
DYNAMODB_ENDPOINT=http://localhost:4569
DYNAMOB_TABLENAME=hkstars

"${DC[@]}" down
"${DC[@]}" rm
"${DC[@]}" build

# Set up environment
"${DC[@]}" up -d localstack
timeout=$(( $( date '+%s' ) + 120 ))
echo -n 'waiting for localstack'
until "${DC[@]}" logs localstack 2>&1 | grep -q -m 1 "Ready."; do
  if [ $( date '+%s' ) -ge $timeout ]; then
    echo 'TIMEOUT!'
    exit 1
  fi
  sleep 0.5
done
echo ' done'

echo 'create s3 bucket'
aws s3api create-bucket --endpoint-url ${S3_ENDPOINT} --bucket ${S3_BUCKET}

echo 'create dynamodb table'
aws --endpoint-url=${DYNAMODB_ENDPOINT} dynamodb create-table --table-name ${DYNAMOB_TABLENAME} --attribute-definitions AttributeName=star_identifier,AttributeType=S --key-schema AttributeName=star_identifier,KeyType=HASH --provisioned-throughput ReadCapacityUnits=5,WriteCapacityUnits=5

"${DC[@]}" up -d face_recognition
echo -n 'waiting for face_recognition'
until "${DC[@]}" logs face_recognition 2>&1 | grep -q -m 1 "Running on"; do
  if [ $( date '+%s' ) -ge $timeout ]; then
    echo 'TIMEOUT!'
    exit 1
  fi
  sleep 0.5
done
echo ' done'

echo 'populate data'
curl -XPOST -F "file=@example-images/known/aaron.jpg" -F "name=Aaron Kwok" -F "blah=456" http://127.0.0.1:5001/known_faces
curl -XPOST -F "file=@example-images/known/jacky.jpg" -F "name=Jacky Cheung" -F "blah=123" http://127.0.0.1:5001/known_faces
